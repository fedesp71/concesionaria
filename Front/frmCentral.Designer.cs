﻿namespace Front
{
    partial class frmCentral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.formsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMAutosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMVentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // formsToolStripMenuItem
            // 
            this.formsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMClientesToolStripMenuItem,
            this.aBMAutosToolStripMenuItem,
            this.aBMVentasToolStripMenuItem});
            this.formsToolStripMenuItem.Name = "formsToolStripMenuItem";
            this.formsToolStripMenuItem.Size = new System.Drawing.Size(61, 24);
            this.formsToolStripMenuItem.Text = "Forms";
            // 
            // aBMClientesToolStripMenuItem
            // 
            this.aBMClientesToolStripMenuItem.Name = "aBMClientesToolStripMenuItem";
            this.aBMClientesToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.aBMClientesToolStripMenuItem.Text = "ABM_Clientes";
            this.aBMClientesToolStripMenuItem.Click += new System.EventHandler(this.aBMClientesToolStripMenuItem_Click);
            // 
            // aBMAutosToolStripMenuItem
            // 
            this.aBMAutosToolStripMenuItem.Name = "aBMAutosToolStripMenuItem";
            this.aBMAutosToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.aBMAutosToolStripMenuItem.Text = "ABM_Autos";
            this.aBMAutosToolStripMenuItem.Click += new System.EventHandler(this.aBMAutosToolStripMenuItem_Click);
            // 
            // aBMVentasToolStripMenuItem
            // 
            this.aBMVentasToolStripMenuItem.Name = "aBMVentasToolStripMenuItem";
            this.aBMVentasToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.aBMVentasToolStripMenuItem.Text = "ABM_Ventas";
            this.aBMVentasToolStripMenuItem.Click += new System.EventHandler(this.aBMVentasToolStripMenuItem_Click);
            // 
            // frmCentral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmCentral";
            this.Text = "frmCentral";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem formsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMAutosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMVentasToolStripMenuItem;
    }
}