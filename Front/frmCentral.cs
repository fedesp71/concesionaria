﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Front
{
    public partial class frmCentral : Form
    {
        public frmCentral()
        {
            InitializeComponent();
        }

        private void aBMVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Venta frmVenta = new Venta();
            frmVenta.MdiParent = this;
            frmVenta.Show();
        }

        private void aBMClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCliente frmCliente = new frmCliente();
            frmCliente.MdiParent = this;
            frmCliente.Show();
        }

        private void aBMAutosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAuto frmAuto = new frmAuto();
            frmAuto.MdiParent = this;
            frmAuto.Show();
        }
    }
}
