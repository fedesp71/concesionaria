﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Front
{
    public partial class Venta : Form
    {
        public Venta()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarAutos();
        }


        public void CargarAutos()
        {

            //List<Auto> autitos = new List<Auto>();

        

            List<Auto> autitos = Negocio.Auto.ListarAutos();
            int index = 0;
            foreach(Auto auto in autitos)
            {
                
                dtAutos.Rows[index].Cells["Patente"].Value = auto.Patente.ToString();
                dtAutos.Rows[index].Cells["Anio"].Value    = auto.Anio.ToString();
                dtAutos.Rows[index].Cells["Marca"].Value   = auto.Marca.ToString();
                dtAutos.Rows[index].Cells["Modelo"].Value  = auto.Modelo.ToString();
                dtAutos.Rows[index].Cells["Precio"].Value  = auto.Precio.ToString();

                index++;

            }
        }
    }
}
