﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Front
{
    public partial class frmCliente : Form
    {
        public frmCliente()
        {
            InitializeComponent();
        }

        private void btnAgregarCliente_Click(object sender, EventArgs e)
        {
            Cliente cli = new Cliente();
            cli.Dni = int.Parse(txtDni.Value.ToString());
            cli.NyApe = txtNyApe.Text;

            Negocio.Cliente.CargarCliente(int.Parse(txtDni.Value.ToString()), txtNyApe.Text);

            CargarClientes();
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {
            CargarClientes();
        }

        public void CargarClientes()
        {

            dtClientes.Rows.Clear();
            List<Cliente> clientes = Negocio.Cliente.ListarClientes();

            int index = 0;
            foreach(Cliente clienteAux in clientes)
            {
                dtClientes.Rows[index].Cells["Dni"].Value = clienteAux.Dni.ToString();
                dtClientes.Rows[index].Cells["NombreApellido"].Value = clienteAux.NyApe;

                index++;
            }
        }
    }
}
