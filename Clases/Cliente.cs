﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Clases;
namespace Clases
{
    public class Cliente
    {
        private int dni;

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }

        private string nyApe;

        public string NyApe
        {
            get { return nyApe; }
            set { nyApe = value; }
        }


        public static List<Cliente> ListarClientes()
        {
            List<Cliente> clientes = new List<Cliente>();
            Acceso acceso = new Acceso();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spObtenerClientes");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Cliente cliente = new Cliente();
                cliente.Dni = int.Parse(registro["Dni"].ToString());
                cliente.NyApe = registro["NyApe"].ToString();

                clientes.Add(cliente);
            }

            return clientes;
        }

    }
}
