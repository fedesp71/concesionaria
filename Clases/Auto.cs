﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
namespace Clases
{
    public class Auto
    {
        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private int anio;

        public int Anio
        {
            get { return anio; }
            set { anio = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private string modelo;

        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        private int precio;

        public int Precio
        {
            get { return precio; }
            set { precio = value; }
        }


        public static List<Auto> ListarAutos()
        {
            List<Auto> autos = new List<Auto>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Genero_Listar");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Auto auto    = new Auto();
                auto.Patente = registro["Patente"].ToString();
                auto.Anio    = int.Parse(registro["Anio"].ToString());
                auto.Marca   = registro["Marca"].ToString();
                auto.Modelo  = registro["Modelo"].ToString();
                auto.Precio  = int.Parse(registro["Precio"].ToString());

                autos.Add(auto);
            }

            return autos;
        }

    }
}
