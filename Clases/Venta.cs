﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Clases
{
    public class Venta
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private List<Auto> autos = new List<Auto>();

        public List<Auto> Autos
        {
            get { return autos = new List<Auto>(); }
            set { autos = new List<Auto>(); }
        }

        private int valorFinal;

        public int ValorFinal
        {
            get { return valorFinal; }
            set { valorFinal = value; }
        }


        public int CalcularValorFinal(List<Auto> autosAux)
        {
            int valor = 0;

            foreach (Auto auto in autosAux)
            {
                valor += auto.Precio;
            }

            return valor;
        }


        public static void GenerarVenta(Cliente clienteAux, List<Auto> autosAux)
        {
            Venta ventaAux = new Venta();
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("DniCliente", clienteAux.Dni));
            string autosParam = "";
            foreach(Auto auto in autosAux)
            {
                autosParam += auto.Patente.ToString() + "|";
            }
            acceso.Abrir();
            int filas = acceso.Escribir("Genero_Listar", parametros);
            acceso.Cerrar();
        }
    }
}
