﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clases
{
    class ConcesionariaAutos
    {
        private List<Auto> autos = new List<Auto>();

        public List<Auto> Autos
        {
            get { return autos = new List<Auto>(); }
            set { autos = new List<Auto>(); }
        }

        private List<Cliente> clientes = new List<Cliente>();

        public List<Cliente> Clientes
        {
            get { return clientes = new List<Cliente>(); }
            set { clientes = new List<Cliente>(); }
        }

        private List<Venta> ventas = new List<Venta>();

        public List<Venta> Ventas
        {
            get { return ventas = new List<Venta>(); }
            set { ventas = new List<Venta>(); }
        }


        public void AgregarCliente(int dni, string nApe)
        {
            Cliente clienteAux = new Cliente();
            clienteAux.Dni = dni;
            clienteAux.NyApe = nApe;

            Clientes.Add(clienteAux);
             
        }


        public void AgregarAuto(string patente, int anio, string marca, string modelo, int precio)
        {
            Auto autoAux = new Auto();

            autoAux.Patente = patente;
            autoAux.Anio    = anio;
            autoAux.Marca   = marca;
            autoAux.Modelo  = modelo;
            autoAux.Precio  = precio;

            Autos.Add(autoAux);
        }

        public void GenerarVenta(Cliente clienteAux, List<Auto> autosAux)
        {
            Venta ventaAux = new Venta();

            ventaAux.Cliente = clienteAux;
            ventaAux.Autos = autosAux;
            ventaAux.ValorFinal = ventaAux.CalcularValorFinal(autosAux);
        }

    }
}
