﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Negocio
{
    public class Auto
    {
        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private int anio;

        public int Anio
        {
            get { return anio; }
            set { anio = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private string modelo;

        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        private int precio;

        public int Precio
        {
            get { return precio; }
            set { precio = value; }
        }


        public static List<Auto> ListarAutos()
        {
            List<Auto> autos = new List<Auto>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("spObtenerAutos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Auto auto = new Auto();
                auto.Patente = registro["Patente"].ToString();
                auto.Anio = int.Parse(registro["Anio"].ToString());
                auto.Marca = registro["Marca"].ToString();
                auto.Modelo = registro["Modelo"].ToString();
                auto.Precio = int.Parse(registro["Precio"].ToString());

                autos.Add(auto);
            }

            return autos;
        }

        public static void CargarAuto(string patente, int anio, string marca, string modelo, int precio)
        {
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("Patente", patente));
            parametros.Add(acceso.CrearParametro("Anio", anio));
            parametros.Add(acceso.CrearParametro("Marca", marca));
            parametros.Add(acceso.CrearParametro("Modelo", modelo));
            parametros.Add(acceso.CrearParametro("Precio", precio));

            int filas = acceso.Escribir("spAgregarAuto", parametros);
            acceso.Cerrar();
        }




        public static void EliminarAuto(string patente)
        {
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("Patente", patente));


            int filas = acceso.Escribir("spEliminarAuto", parametros);
            acceso.Cerrar();
        }

    }
}
